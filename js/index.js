(function(){
  Vue.component('countdown', {
    template: '#countdown-template',
    mounted() {
      window.setInterval(() => {
        this.now = Math.trunc((new Date()).getTime() / 1000)
      }, 1000)
    },
    props: {
      date: {
        type: String
      }
    },
    data() {
      return {
        now: Math.trunc((new Date()).getTime() / 1000)
      }
    },
    computed: {
      dateInMilliseconds() {
        return Math.trunc(Date.parse(this.date) / 1000)
      },
      seconds() {
        return (this.dateInMilliseconds - this.now) % 60
      },
      minutes() {
        return Math.trunc((this.dateInMilliseconds - this.now) / 60) % 60
      },
      hours() {
        return Math.trunc((this.dateInMilliseconds - this.now) / 60 / 60) % 24
      },
      days() {
        return Math.trunc((this.dateInMilliseconds - this.now) / 60 / 60 / 24)
      }
    }
  })
  
  Vue.filter('two_digits', (value) => {
    if (value < 0) {
      return '00'
    }
    if (value.toString().length <= 1) {
      return `0${value}`
    }
    return value
  })
  
  const countdown = new Vue({
    el: '#countdown'
  })
  
  Vue.component('step', {
    template: '#step-template',
    props: {
      current: {
        type: Number
      },
      max: {
        type: Number
      }
    },
    computed: {
      fillProgress() {
        return `${Math.trunc(parseInt(this.current) / parseInt(this.max) * 100)}%`
      }
    }
  })
  
  const step = new Vue({
    el: '#step'
  })

  document.addEventListener('DOMContentLoaded', function(){
    const copyBlock = document.querySelector('#copyblock')
    const copyBtn = document.querySelector('#copybtn')

    if (copyBlock && copyBtn) {
      copyBtn.addEventListener('click', () => copyToClipboard(copyBlock.innerHTML))
    }

    function copyToClipboard (text) {
      if (window.clipboardData && window.clipboardData.setData) {
          return clipboardData.setData("Text", text); 
  
      } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
          let textarea = document.createElement("textarea");
          textarea.textContent = text;
          textarea.style.position = "fixed";
          document.body.appendChild(textarea);
          textarea.select();
          try {
              return document.execCommand("copy");
          } catch (ex) {
              console.warn("Copy to clipboard failed.", ex);
              return false;
          } finally {
              document.body.removeChild(textarea);
          }
      }
    }
  })
})()


